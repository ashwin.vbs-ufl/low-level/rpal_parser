#include "RPALLexer.h"
#include <cctype>

namespace RPAL{

//function to process token held by temp cache as token type specified in argument.
Token Lexer::processToken(TokenType type){
    //if an identifier matches a rpal keyword, mark it as keyword type.
    if(type == TokenType::IDENTIFIER){
        if( temp == "let"   ||
            temp == "in"    ||
            temp == "fn"    ||
            temp == "where" ||
            temp == "aug"   ||
            temp == "or"    ||
            temp == "not"   ||
            temp == "gr"    ||
            temp == "ls"    ||
            temp == "ge"    ||
            temp == "le"    ||
            temp == "eq"    ||
            temp == "ne"    ||
            temp == "true"  ||
            temp == "false" ||
            temp == "nil"   ||
            temp == "dummy" ||
            temp == "within" ||
            temp == "and"   ||
            temp == "rec")
            type = TokenType::KEYWORD;
    }

    //construct token and reset temporary cache, line and column numbers
    Token result(type, temp, lineNum, colNum);
    temp = "";
    lineNum = currentLine;
    colNum = currentCol;

    return result;
}

//rule for identifiers.
Token Lexer::Rule_Identifier(){
    //starts with alphabet,
    Rule_Letter();

    char c = input.peek();
    //continue while next character is a alphabet or number or underscore
    while(std::isalpha(c) || std::isdigit(c) || c=='_'){
        if(std::isalpha(c))
            Rule_Letter();
        else if(std::isdigit(c))
            Rule_Digit();
        else if(c == '_'){
            temp.append(1, input.get());
            currentCol++;
        }
        c = input.peek();
    }
    return processToken(TokenType::IDENTIFIER);
}

//rule for integers
Token Lexer::Rule_Integer(){
    //starts with digit
    Rule_Digit();

    //continue while next character is also a digit.
    while(std::isdigit(input.peek())){
        Rule_Digit();
    }
    return processToken(TokenType::INTEGER);
}

//rule for token that starts with a '/' - its either a div operator or a comment
Token Lexer::Rule_DivOrComment(){
    //store the first '/'
    temp.append(1, input.get());
    currentCol++;

    //if the next character is also a '/' process comment. else return a div operator.
    if(input.peek() == '/')
        return Rule_Comment();
    else
        return processToken(TokenType::OPERATOR);
}

//rule for processing operators.
Token Lexer::Rule_Operator(){
    char c = input.get();
    temp.append(1, c);
    currentCol++;

    switch(c){
        // if first character is either gr or le, we have to check the next character to differentiate between gr and ge or le and ls.
        case '>':
        case '<':
            if(input.peek() == '='){
                temp.append(1, input.get());
                currentCol++;
            }
        break;

        // if first character of operator is '-' we have to check next character to differentiate '-' from '->'
        case '-':
            if(input.peek() == '>'){
                temp.append(1, input.get());
                currentCol++;
            }
        break;

        // if first character of operator is '*' we have to check next character to differentiate '*' from '**'
        case '*':
            if(input.peek() == '*'){
                temp.append(1, input.get());
                currentCol++;
            }
        break;
    }
    return processToken(TokenType::OPERATOR);
}

Token Lexer::Rule_String(){
    do{
        char c = input.get();

        // check that string characters are valid. escaped characters are not checked as they are handled downstream
        if(!std::isalpha(c) && !std::isdigit(c) && !isPunction(c) && !(c == ' ') && !(c == '\\') && !isOperator(c) && !(c == '\'')){
            throw "unexpected string literal";
        }

        temp.append(1, c);
        currentCol++;

        //handle escaped characters.
        if(c == '\\'){
            c = input.get();
            //allowed escaped characters are \\, \t, \n and \'
            if( (c == '\\') ||
                (c == 't') ||
                (c == 'n') ||
                (c == '\''))
            {
                temp.append(1, c);
                currentCol++;
            } else {
                throw "unrecognized escape sequence";
            }
        }
    } while(input.peek() != '\''); //continue while the next character is not the close quote
    temp.append(1, input.get()); //get the close quote
    return processToken(TokenType::STRING);
}

//rule for handling spaces tabs and next line characters.
Token Lexer::Rule_Spaces(){
    do{
        char c = input.get();

        //increment line and column tracker based on what kind of space character is.
        switch(c){
            case ' ':
            case '\t':
                currentCol++;
            break;
            case '\n':
                currentCol = 0;
                currentLine++;
            break;
        }
        temp.append(1, c);
    } while (isSpace(input.peek()));

    return processToken(TokenType::DELETE);
}

//rule for handling comments.
Token Lexer::Rule_Comment(){
    do{
        char c = input.get();
        if(!std::isdigit(c) && !std::isalpha(c) && !isPunction(c) && !(c == '\\') && !(c == '\'') && !isOperator(c) && !isSpace(c)){
            throw "unrecognized string literal within comment";
        }
        temp.append(1, c);
        currentCol++;
    } while(input.peek() != '\n'); //read till end of line.
    return processToken(TokenType::DELETE);
}

//rule for handling punctions.
Token Lexer::Rule_Punction(){
    temp.append(1, input.get());
    currentCol++;
    return processToken(TokenType::PUNCTION);
}

//rule for handling alphabets
void Lexer::Rule_Letter(){
    temp.append(1, input.get());
    currentCol++;
}

//rule for handling digits
void Lexer::Rule_Digit(){
    temp.append(1, input.get());
    currentCol++;
}

//check whether the character could signify the start of div operator or comment.
bool Lexer::isDivOrComment(char c){
    return (c == '/');
}

//check whether the character could signify the start of an operator.
bool Lexer::isOperator(char c){
    return ((c == '|')||
            (c == '&')||
            (c == '>')||
            (c == '=')||
            (c == '<')||
            (c == '+')||
            (c == '-')||
            (c == '*')||
            (c == '/')||
            (c == '@')||
            (c == '.')||
            (c == ':')||
            (c == '~')||
            (c == '$')||
            (c == '!')||
            (c == '#')||
            (c == '%')||
            (c == '^')||
            (c == '_')||
            (c == '[')||
            (c == ']')||
            (c == '{')||
            (c == '}')||
            (c == '`')||
            (c == '?')||
            (c == '\"'));
}

//check whether the character could signify the start of a punction.
bool Lexer::isPunction(char c){
    return ((c == '(') ||
            (c == ')') ||
            (c == ';') ||
            (c == ','));
}

//check whether the character could signify the start of a string.
bool Lexer::isString(char c){
    return ((c == '\''));
}

//check whether the character could signify the start of a space.
bool Lexer::isSpace(char c){
    return ((c == ' ')||
            (c == '\t')||
            (c == '\n'));
}

//lexer constructor. initialize line, column to zero and create the ifstream object.
Lexer::Lexer(std::string file) :
    input(file.c_str()),
    lineNum(0),
    colNum(0),
    currentLine(0),
    currentCol(0),
    temp() {
    //poptoken is called to initialize the token cache.
    popToken();
}

//functin to get next token.
Token Lexer::getToken(){
    //check the first character of stream to figure out what kind of token might start with it. and call associated rule.
    char c = input.peek();
    if(std::isalpha(c))
        return Rule_Identifier();
    else if(std::isdigit(c))
        return Rule_Integer();
    else if(isDivOrComment(c))
        return Rule_DivOrComment();
    else if(isOperator(c))
        return Rule_Operator();
    else if(isString(c))
        return Rule_String();
    else if(isSpace(c))
        return Rule_Spaces();
    else if(isPunction(c))
        return Rule_Punction();
    else if(c == EOF)
        return Token(TokenType::END_OF_FILE, "", currentLine, currentCol);
    else
        throw "unexpected token at line";
}

//peeks into the token cache.
Token Lexer::peekToken(){
    return cache;
}

//clears the token cache and populates it with the next token.
void Lexer::popToken(){
    do{
        cache = getToken();
    } while(cache.type == TokenType::DELETE);
}

//read is a function used by parser to pop a token while asserting its type and content.
bool Lexer::read(TokenType type, std::string text){
    if((cache.type == type) && (text == cache.str)){
        popToken();
        return true;
    } else
        throw "something wrong";
}

}
