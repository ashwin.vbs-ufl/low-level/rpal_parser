#include "RPALParser.h"
#include <stack>
#include <map>
#include <queue>

namespace RPAL{

class Env;

class CSENode{
public:
    NodeType type;

    std::string strValue;
    int intValue;
    std::stack<CSENode> tauValue;

    AST_Node* node;
    Env* baseEnv;

    bool isPrimed;

    CSENode(NodeType mType):
        type(mType),
        strValue(),
        intValue(0),
        node(nullptr),
        baseEnv(nullptr),
        isPrimed(false){}
};

class Env{
private:
    Env* parent;
    std::map<std::string, CSENode> variables;
public:
    std::stack<CSENode> RStack;
    Env(Env* p): parent(p) {}
    CSENode lookup(std::string);
    bool isStored(std::string key);
    void store(std::string, CSENode);
};



class CSE{
private:
    void processChildren(AST_Node* node, Env& environment);

    void processLAMBDA(AST_Node* node, Env& environment); // remember to ignore brkt
    void processTAU(AST_Node* node, Env& environment);
    void processARROW(AST_Node* node, Env& environment);
    void processBINARY(NodeType type, Env& environment);
    void processUNARY(NodeType type, Env& environment);
    void processIDENTIFIER(std::string name, Env& environment);
    void processINTEGER(int value, Env& environment);
    void processSTRING(std::string value, Env& environment);
    void processDUMMY(NodeType type, Env& environment);
    void processGAMMA(Env& environment);

    void processNode(AST_Node* node, Env& environment);
    void printNode(CSENode& node);
    void printElements(std::stack<CSENode>& list);

    void indexLambdas(AST_Node*);
    void indexLambdasSubtree(AST_Node* node, int&, std::queue<AST_Node*>&);
public:
    CSENode processTree(AST_Node*);
};

}
