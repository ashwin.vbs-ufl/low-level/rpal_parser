#include "RPALLexer.h"

namespace RPAL{

//node types as described by the RPAL grammar
enum class NodeType : int {
    LET,
    LAMBDA,
    WHERE,
    TAU,
    AUG,
    ARROW,
    B_OR,
    B_AND,
    B_NOT,
    B_GR,
    B_GE,
    B_LS,
    B_LE,
    B_EQ,
    B_NE,
    ADD,
    SUB,
    NEG,
    MULT,
    DIV,
    POW,
    AT,
    GAMMA,
    R_TRUE,
    R_FALSE,
    R_NIL,
    R_DUMMY,
    D_WITHIN,
    D_AND,
    D_REC,
    D_EQ,
    D_FCN,
    BRKT,
    COMMA,
    IDENTIFIER,
    INTEGER,
    STRING,
    YSTAR,
    ETA
};

// AST node structures - contains node data, pointer to leftmost child. Sibling nodes are stored as a doubly linked lists.
// AST can be traverser top to down and sideways. child to parent node traversal not programmed, as it is not necessary as of now.
class AST_Node{
public:
    NodeType type;
    std::string text;

    AST_Node* next;
    AST_Node* prev;
    AST_Node* leftchild;

    int lIndex;

    ~AST_Node(){
        delete next;
        delete leftchild;
    }

    AST_Node(){
        next = nullptr;
        prev = nullptr;
        leftchild = nullptr;
    }
};

// parser class instantiates lexer, manages AST and contains all the rules of the grammar.
// buildAST function returns a AST node pointer to the root of the AST. Tree is allocated on the heap and managed by the Parser. The structure should not be modified or freed outside.
class Parser{
private:
    AST_Node root;
    AST_Node* end;

    std::string file;
    Lexer* lexer;

    void pushNode(NodeType type, std::string text);
    void buildTree(NodeType, int numNodes);

    void Rule_E();
    void Rule_Ew();
    void Rule_T();
    void Rule_Ta();
    void Rule_Tc();
    void Rule_B();
    void Rule_Bt();
    void Rule_Bs();
    void Rule_Bp();
    void Rule_A();
    void Rule_At();
    void Rule_Af();
    void Rule_Ap();
    void Rule_R();
    void Rule_Rn();
    void Rule_D();
    void Rule_Da();
    void Rule_Dr();
    void Rule_Db();
    void Rule_Vb();
    void Rule_Vl();
    void Rule_Identifier();
    void Rule_Integer();
    void Rule_String();

    void optimizeNode(AST_Node*);

public:
    Parser(std::string path) : end(&root), file(path) {}
    AST_Node* buildAST();

    AST_Node* optimize();

    ~Parser();
};

}
