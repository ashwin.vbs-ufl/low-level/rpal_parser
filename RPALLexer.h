#include <string>
#include <fstream>

namespace RPAL{

//token types as described by the RPAL lexicon.
enum class TokenType : int {
    IDENTIFIER,
    KEYWORD, //keyword token type has been introduced to account for identifiers that are inbuilt into RPAL as part of its grammar.
    INTEGER,
    OPERATOR,
    STRING,
    DELETE,
    PUNCTION,
    END_OF_FILE
};

// structure that describes a token. line number and coNumber are additional metadata that are stored for ease of debugging.
class Token{
public:
    Token(){};

    Token(TokenType m_type, std::string m_str, int line, int col):
        type(m_type),
        str(m_str),
        lineNum(line),
        colNum(col) {}

    TokenType type;
    std::string str;
    int lineNum;
    int colNum;
};

// lexer is structured very much like a parser, with distinct rules.
// but unlike a parser, it pauses whenever a full token has been read and not till EOF.
// lexer has to be instantiated once every full traversal of a file -there is no way to reset the input stream while using lexer, as of now.
class Lexer{
private:
    std::ifstream input;
    int lineNum, colNum;
    int currentLine, currentCol;

    std::string temp;

    Token Rule_Identifier();
    Token Rule_Integer();

    bool isDivOrComment(char c);
    Token Rule_DivOrComment();
    Token Rule_Comment();

    bool isOperator(char c);
    Token Rule_Operator();

    bool isString(char c);
    Token Rule_String();

    bool isSpace(char c);
    Token Rule_Spaces();

    bool isPunction(char c);
    Token Rule_Punction();

    void Rule_Letter();

    void Rule_Digit();


    Token processToken(TokenType);

    Token cache;

public:
    Lexer(std::string file);
    Token getToken();

    Token peekToken();
    void popToken();
    bool read(TokenType type, std::string text);
};

}
