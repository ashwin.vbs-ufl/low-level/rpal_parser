#include "RPALParser.h"

namespace RPAL{

//function to push a terminal node onto the root level of the AST. this level is used only during AST construction.
void Parser::pushNode(NodeType type, std::string text){
    AST_Node* node = new AST_Node();
    node->type = type;
    node->text = text;

    end->next = node;
    node->prev = end;
    node->next = nullptr;
    node->leftchild = nullptr;
    end = node;
}

//function detatches the last <numNodes> nodes from the root level of tree and pushes a new node of type <type> while adding the detatched nodes as children of the new node.
void Parser::buildTree(NodeType type, int numNodes){
    AST_Node* child;
    child = end;
    numNodes--;
    // go back <numNodes> nodes from the end.
    while(numNodes){
        child = child->prev;
        numNodes--;
    }

    //detatch the last nodes.
    end = child->prev;
    child->prev = nullptr;

    // push a new node of type <type>
    pushNode(type, "");

    //re attach the detatched nodes as the new node's child
    end->leftchild = child;
}

//rule for Expression type E
void Parser::Rule_E(){
    Token temp = lexer->peekToken();
    if(temp.str == "let"){
        //process rule for let if next token is let.
        lexer->read(TokenType::KEYWORD, "let");
        Rule_D();
        lexer->read(TokenType::KEYWORD, "in");
        Rule_E();
        buildTree(NodeType::LET, 2);
    } else if(temp.str == "fn") {
        //process rule for lambda if next token is fn
        int nodes = 1;
        lexer->read(TokenType::KEYWORD, "fn");
        do{
            Rule_Vb();
            nodes++;
        } while(lexer->peekToken().str != ".");
        lexer->read(TokenType::OPERATOR, ".");
        Rule_E();
        buildTree(NodeType::LAMBDA, nodes);
    } else {
        //else parse the rest of stream as a Ew expression
        Rule_Ew();
    }
}

//rule for Expression type Ew
void Parser::Rule_Ew(){
    Rule_T(); //process first Tuple expression

    if(lexer->peekToken().str == "where"){
        //process rule where if next token is where
        lexer->read(TokenType::KEYWORD, "where");
        Rule_Dr();
        buildTree(NodeType::WHERE, 2);
    }
}

//rule for Tuple Expression type T
void Parser::Rule_T(){
    int nodes = 1;
    Rule_Ta(); //process first Ta expression

    while(lexer->peekToken().str == ","){
        // continue parsing Ta expressions separated by commas
        lexer->read(TokenType::PUNCTION, ",");
        Rule_Ta();
        nodes++;
    }

    //build tau node if multiple Ta expressions have been parsed.
    if(nodes > 1)
        buildTree(NodeType::TAU, nodes);
}

//rule for Tuple expression type Ta
void Parser::Rule_Ta(){
    Rule_Tc(); //process first Tc expression

    while(lexer->peekToken().str == "aug"){
        //parse rule aug if next read token is aug
        lexer->read(TokenType::KEYWORD, "aug");
        Rule_Tc();
        buildTree(NodeType::AUG, 2);
    }
}

//rule for Tuple expression type Tc
void Parser::Rule_Tc(){
    Rule_B(); //process first B expression

    if(lexer->peekToken().str == "->"){
        //parse rule -> if next read token is ->
        lexer->read(TokenType::OPERATOR, "->"); //
        Rule_Tc();
        lexer->read(TokenType::OPERATOR, "|");
        Rule_Tc();
        buildTree(NodeType::ARROW, 3);
    }
}

//rule for Boolean expression type B
void Parser::Rule_B(){
    Rule_Bt(); //process first Bt expression

    while(lexer->peekToken().str == "or"){
        //process rule or if next read token is or
        lexer->read(TokenType::KEYWORD, "or");
        Rule_Bt();
        buildTree(NodeType::B_OR, 2);
    }
}

//rule for Boolean expression type Bt
void Parser::Rule_Bt(){
    Rule_Bs(); // process first Bs expression

    while(lexer->peekToken().str == "&"){
        //process rule & if next read token is &
        lexer->read(TokenType::OPERATOR, "&");
        Rule_Bs();
        buildTree(NodeType::B_AND, 2);
    }
}

//rule for Boolean expression type Bs
void Parser::Rule_Bs(){
    if(lexer->peekToken().str == "not"){
        // if the first token is not, process not rule
        lexer->read(TokenType::KEYWORD, "not");
        Rule_Bp();
        buildTree(NodeType::B_NOT, 1);
    } else
        Rule_Bp();
}

//rule for Boolean expression type Bp
void Parser::Rule_Bp(){
    Rule_A(); //process the first A expression

    Token token = lexer->peekToken();

    //depending on the next token, process either a gr, ge, ls, le, eq or ne node.
    if(token.str == "gr"){
        lexer->read(TokenType::KEYWORD, "gr");
        Rule_A();
        buildTree(NodeType::B_GR, 2);
    } else if(token.str == ">"){
        lexer->read(TokenType::OPERATOR, ">");
        Rule_A();
        buildTree(NodeType::B_GR, 2);
    }

    else if(token.str == "ge"){
        lexer->read(TokenType::KEYWORD, "ge");
        Rule_A();
        buildTree(NodeType::B_GE, 2);
    } else if(token.str == ">="){
        lexer->read(TokenType::OPERATOR, ">=");
        Rule_A();
        buildTree(NodeType::B_GE, 2);
    }

    else if(token.str == "ls"){
        lexer->read(TokenType::KEYWORD, "ls");
        Rule_A();
        buildTree(NodeType::B_LS, 2);
    } else if(token.str == "<"){
        lexer->read(TokenType::OPERATOR, "<");
        Rule_A();
        buildTree(NodeType::B_LS, 2);
    }

    else if(token.str == "le"){
        lexer->read(TokenType::KEYWORD, "le");
        Rule_A();
        buildTree(NodeType::B_LE, 2);
    } else if(token.str == "<="){
        lexer->read(TokenType::OPERATOR, "<=");
        Rule_A();
        buildTree(NodeType::B_LE, 2);
    }

    else if(token.str == "eq"){
        lexer->read(TokenType::KEYWORD, "eq");
        Rule_A();
        buildTree(NodeType::B_EQ, 2);
    }

    else if(token.str == "ne"){
        lexer->read(TokenType::KEYWORD, "ne");
        Rule_A();
        buildTree(NodeType::B_NE, 2);
    }
}

//rule for Arithmetic expression type A
void Parser::Rule_A(){
    Token token = lexer->peekToken();
    if(token.str == "+"){
        //if the first token is a + sign, ignore and process the rest as a At expression
        lexer->read(TokenType::OPERATOR, "+");
        Rule_At();
    } else if(token.str == "-"){
        //if the first token is a - sign, process the rest as At expression as build a neg node.
        lexer->read(TokenType::OPERATOR, "-");
        Rule_At();
        buildTree(NodeType::NEG, 1);
    } else {
        Rule_At(); //process the first At expression

        token = lexer->peekToken();

        //depending on the next token, process either a + rule or a - rule repeatedly.
        while(token.str == "+" || token.str == "-"){
            if(token.str == "+"){
                lexer->read(TokenType::OPERATOR, "+");
                Rule_At();
                buildTree(NodeType::ADD, 2);
            } else if(token.str == "-"){
                lexer->read(TokenType::OPERATOR, "-");
                Rule_At();
                buildTree(NodeType::SUB, 2);
            }
            token = lexer->peekToken();
        }
    }
}

//rule for Arithmetic expression type At
void Parser::Rule_At(){
    Rule_Af(); //parse the first Af expression

    Token token = lexer->peekToken();
    //depending on the next token, process either a * rule or a / rule repeatedly.
    while(token.str == "*" || token.str == "/"){
        if(token.str == "*"){
            lexer->read(TokenType::OPERATOR, "*");
            Rule_Af();
            buildTree(NodeType::MULT, 2);
        } else if(token.str == "/"){
            lexer->read(TokenType::OPERATOR, "/");
            Rule_Af();
            buildTree(NodeType::DIV, 2);
        }
        token = lexer->peekToken();
    }
}

//rule for Arithmetic expression type Af
void Parser::Rule_Af(){
    Rule_Ap(); //parse the first Ap expression

    //if the next token is ** build a pow node
    if(lexer->peekToken().str == "**"){
        lexer->read(TokenType::OPERATOR, "**");
        Rule_Af();
        buildTree(NodeType::POW, 2);
    }
}

//rule for Arithmetic expression type Ap
void Parser::Rule_Ap(){
    Rule_R(); //parse the first R expression

    //while the next node is a @ character, parse the @ rule repeatedly.
    while(lexer->peekToken().str == "@"){
        lexer->read(TokenType::OPERATOR, "@");
        Rule_Identifier();
        Rule_R();
        buildTree(NodeType::AT, 3);
    }
}

//rule for Rator and Rand expression type R
void Parser::Rule_R(){
    Rule_Rn(); //parse the first Rn rule

    Token temp = lexer->peekToken();
    //while the next token is a valid Rn expression (determined by peeking for the next token) repeatedly parse for the gamma rule.
    while(  temp.type == TokenType::INTEGER ||
            temp.type == TokenType::STRING ||
            temp.type == TokenType::IDENTIFIER ||
            (temp.type == TokenType::KEYWORD && (
                temp.str == "true" ||
                temp.str == "false" ||
                temp.str == "nil" ||
                temp.str == "dummy"
            )) ||
            (temp.type == TokenType::PUNCTION &&
                temp.str == "("
            )){
        Rule_Rn();
        buildTree(NodeType::GAMMA, 2);
        temp = lexer->peekToken();
    }
}

//rule for Rator and Rand expression type Rn
void Parser::Rule_Rn(){
    Token token = lexer->peekToken();
    //based on the token type, instert a node in the root level of AST.
    if(token.str == "("){
        lexer->read(TokenType::PUNCTION, "(");
        Rule_E();
        lexer->read(TokenType::PUNCTION, ")");
    } else if(token.type == TokenType::INTEGER){
        pushNode(NodeType::INTEGER, token.str);
        lexer->popToken();
    } else if(token.type == TokenType::STRING){
        pushNode(NodeType::STRING, token.str);
        lexer->popToken();
    } else if(token.str == "true"){
        pushNode(NodeType::R_TRUE, "");
        lexer->read(TokenType::KEYWORD, "true");
    } else if(token.str == "false"){
        pushNode(NodeType::R_FALSE, "");
        lexer->read(TokenType::KEYWORD, "false");
    } else if(token.str == "dummy"){
        pushNode(NodeType::R_DUMMY, "");
        lexer->read(TokenType::KEYWORD, "dummy");
    } else if(token.str == "nil"){
        pushNode(NodeType::R_NIL, "");
        lexer->read(TokenType::KEYWORD, "nil");
    } else
        Rule_Identifier();
}

//rule for Definition type D
void Parser::Rule_D(){
    Rule_Da(); //parse the first Da expression.

    // if the next token is within, process the within rule.
    if(lexer->peekToken().str == "within"){
        lexer->read(TokenType::KEYWORD, "within");
        Rule_D();
        buildTree(NodeType::D_WITHIN, 2);
    }
}

//rule for Definition type Da
void Parser::Rule_Da(){
    int count = 1;
    Rule_Dr(); //parse the first Dr expression

    //while the next token is and, keep parsing Dr expressions.
    while(lexer->peekToken().str == "and"){
        lexer->read(TokenType::KEYWORD, "and");
        Rule_Dr();
        count++;
    }

    //if more than 1 Dr expressions have been parsed, separated by and tokens, build an and node.
    if(count > 1)
        buildTree(NodeType::D_AND, count);
}

//rule for Definition type Dr
void Parser::Rule_Dr(){
    // if the first token is rec, parse the rec rule.
    if(lexer->peekToken().str == "rec"){
        lexer->read(TokenType::KEYWORD, "rec");
        Rule_Db();
        buildTree(NodeType::D_REC, 1);
    } else
        Rule_Db();
}

//rule for Definition type Db
void Parser::Rule_Db(){
    if(lexer->peekToken().str == "("){
        lexer->read(TokenType::PUNCTION, "(");
        Rule_D();
        lexer->read(TokenType::PUNCTION, ")");
    } else {
        int count = 1;
        Rule_Identifier();
        if(lexer->peekToken().type == TokenType::IDENTIFIER ||
                lexer->peekToken().str == "("){
            Token token = lexer->peekToken();
            while(  token.type == TokenType::IDENTIFIER ||
                    token.str == "("
                ){
                count++;
                Rule_Vb();
                token = lexer->peekToken();
            }
            lexer->read(TokenType::OPERATOR, "=");
            Rule_E();
            buildTree(NodeType::D_FCN, count + 1);
        } else {
            while(lexer->peekToken().str == ","){
                lexer->read(TokenType::PUNCTION, ",");
                Rule_Identifier();
                count++;
            }
            if(count > 1)
                buildTree(NodeType::COMMA, count);
            lexer->read(TokenType::OPERATOR, "=");
            Rule_E();
            buildTree(NodeType::D_EQ, 2);
        }
    }
}

//rule for Variable type Vb
void Parser::Rule_Vb(){
    if(lexer->peekToken().str == "("){
        lexer->read(TokenType::PUNCTION, "(");
        if(lexer->peekToken().str == ")"){
            lexer->read(TokenType::PUNCTION, ")");
            pushNode(NodeType::BRKT, "");
        } else {
            Rule_Vl();
            lexer->read(TokenType::PUNCTION, ")");
        }
    } else
        Rule_Identifier();
}

//rule for Variable lists
void Parser::Rule_Vl(){
    int count = 1;
    Rule_Identifier(); //read the first identifier

    //while the next token is a comma, keep reading further identifiers.
    while(lexer->peekToken().str == ","){
        lexer->read(TokenType::PUNCTION, ",");
        Rule_Identifier();
        count++;
    }

    //build a comma node if more than one comma separated identifiers have been read.
    if(count > 1)
        buildTree(NodeType::COMMA, count);
}

//rule for parsing identifiers
void Parser::Rule_Identifier(){
    Token token = lexer->peekToken();
    if(token.type == TokenType::IDENTIFIER){
        pushNode(NodeType::IDENTIFIER, token.str);
        lexer->popToken();
    } else
        throw "something wrong";
}

AST_Node* Parser::buildAST(){
    //clean up any AST built preveously
    delete root.next;
    root.next = nullptr;
    end = &root;

    //initialize lexer
    lexer = new Lexer(file);

    //call the base rule.
    Rule_E();

    //ensure end of file when all tokens have been parsed.
    if(lexer->peekToken().type != TokenType::END_OF_FILE)
        throw "something wrong";
    delete lexer;
    return root.next;
}

Parser::~Parser(){
    //clean up allocated data structures to prevent leaks
    delete root.next;
    root.next = nullptr;
    end = &root;
}

AST_Node* Parser::optimize(){
    optimizeNode(root.next);
    return root.next;
}

void Parser::optimizeNode(AST_Node* node){
    auto child = node->leftchild;
    while(child){
        optimizeNode(child);
        child = child->next;
    }
    switch(node->type){
        case NodeType::LET: {
            auto eq = node->leftchild;
            auto P = eq->next;
            auto X = eq->leftchild;
            auto E = X->next;
            if(P->next != nullptr || E->next != nullptr || eq->type != NodeType::D_EQ)
                throw "leftchild children are all wrong";

            X->next = P; P->prev = X;
            eq->next = E; E->prev = eq;

            node->type = NodeType::GAMMA;
            eq->type = NodeType::LAMBDA;
        } break;
        case NodeType::WHERE: {
            auto P = node->leftchild;
            auto eq = P->next;
            auto X = eq->leftchild;
            auto E = X->next;
            if(eq->next != nullptr || E->next != nullptr || eq->type != NodeType::D_EQ)
                throw "WHERE children are all wrong";

            X->next = P; P->prev = X; P->next = nullptr;
            node->leftchild = eq; eq->prev = nullptr;
            eq->next = E; E->prev = eq;

            node->type = NodeType::GAMMA;
            eq->type = NodeType::LAMBDA;
        } break;
        case NodeType::AT: {
            auto E1 = node->leftchild;
            auto N = E1->next;
            auto E2 = N->next;
            if(E2->next != nullptr || N->type != NodeType::IDENTIFIER)
                throw "AT children are all wrong";

            auto n_node = new AST_Node();
            n_node->type = NodeType::GAMMA;

            node->leftchild = n_node;
            n_node->leftchild = N;
            N->prev = nullptr;
            N->next = E1;
            E1->prev = N;
            E1->next = nullptr;
            n_node->prev = nullptr;
            n_node->next = E2;
            E2->prev = n_node;

            node->type = NodeType::GAMMA;
            n_node->type = NodeType::GAMMA;
        } break;
        case NodeType::D_WITHIN: {
            auto L = node->leftchild;
            auto R = L->next;
            auto X1 = L->leftchild;
            auto E1 = X1->next;
            auto X2 = R->leftchild;
            auto E2 = X2->next;
            if(L->type != NodeType::D_EQ || R->type != NodeType::D_EQ || R->next != nullptr || E1->next != nullptr || E2->next != nullptr)
                throw "WITHIN children are all wrong";

            node->type = NodeType::D_EQ;
            node->leftchild = X2;
            X2->next = R;
            R->leftchild = L;
            R->type = NodeType::GAMMA;
            R->prev = X2;
            E1->prev = L;
            L->next = E1;
            L->type = NodeType::LAMBDA;
            E2->prev = X1;
            X1->next = E2;
        } break;
        case NodeType::D_AND: {
            auto comma = new AST_Node();
            comma->type = NodeType::COMMA;
            auto tau = new AST_Node();
            tau->type = NodeType::TAU;
            tau->prev = comma; comma->next = tau;
            if(node->leftchild->type != NodeType::D_EQ)
                throw "AND children are all wrong";

            comma->leftchild = node->leftchild->leftchild;
            tau->leftchild = node->leftchild->leftchild->next;
            tau->leftchild->prev = nullptr;

            auto x = comma->leftchild;
            auto e = tau->leftchild;
            node->leftchild->leftchild = nullptr;
            if(e->next != nullptr)
                throw "AND children are all wrong";

            for(auto eq = node->leftchild->next; eq != nullptr; eq = eq->next, x = x->next, e = e->next){
                if(eq->type != NodeType::D_EQ)
                    throw "AND children are all wrong";
                auto x1 = eq->leftchild;
                auto e1 = eq->leftchild->next;
                if(e1->next != nullptr)
                    throw "AND children are all wrong";

                x->next = x1; x1->prev = x;
                e->next = e1; e1->prev = e;

                eq->leftchild = nullptr;
            }

            delete node->leftchild;
            x->next = nullptr;

            node->type = NodeType::D_EQ;
            node->leftchild = comma;
        } break;
        case NodeType::D_REC: {
            auto eq = node->leftchild;
            auto X = eq->leftchild;
            auto E = X->next;
            if(eq->type != NodeType::D_EQ || eq->next != nullptr || E->next != nullptr)
                throw "REC children are all wrong";

            auto X1 = new AST_Node();
            X1->type = X->type;
            X1->text = X->text;

            auto Y = new AST_Node();
            Y->type = NodeType::YSTAR;

            auto G = new AST_Node();
            G->type = NodeType::GAMMA;

            node->type = NodeType::D_EQ;
            node->leftchild = X1;
            X1->next = G; G->prev = X1;
            G->leftchild = Y;
            Y->next = eq; eq->prev = Y;
            eq->type = NodeType::LAMBDA;
        } break;
        case NodeType::D_FCN: {
            auto P = node->leftchild;
            for(auto it = P->next; it->next!=nullptr; it=it->next){
                auto L = new AST_Node();
                L->type = NodeType::LAMBDA;

                L->prev = it->prev; it->prev->next = L; L->leftchild = it;
                it->prev = nullptr;
            }
            node->type = NodeType::D_EQ;
        } break;
        default: break;
    }
}

}
