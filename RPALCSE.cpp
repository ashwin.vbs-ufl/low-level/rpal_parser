#include "RPALCSE.h"
#include <sstream>
#include <cmath>
#include <iostream>

namespace RPAL{

CSENode Env::lookup(std::string key){
    auto it = variables.find(key);
    if(it == variables.end())
        return parent->lookup(key);
    else
        return it->second;
}

bool Env::isStored(std::string key){
    auto it = variables.find(key);
    if(it == variables.end()){
        if(parent != nullptr)
            return parent->isStored(key);
        else return false;
    } else
        return true;
}

void Env::store(std::string key, CSENode node){
    variables.insert(make_pair(key, node));
}

void CSE::indexLambdasSubtree(AST_Node* node, int& current, std::queue<AST_Node*>& queue){
    if(node->type == NodeType::LAMBDA){
        current++;
        node->lIndex = current;
        queue.push(node);
    } else {
        if(node->leftchild)
            indexLambdasSubtree(node->leftchild, current, queue);
    }
    if(node->next)
        indexLambdasSubtree(node->next, current, queue);
}

void CSE::indexLambdas(AST_Node* root){
    std::queue<AST_Node*> queue;
    int current = 0;
    indexLambdasSubtree(root, current, queue);
    while(!queue.empty()){
        auto temp = queue.front(); queue.pop();
        indexLambdasSubtree(temp->leftchild->next, current, queue);
    }
}

CSENode CSE::processTree(AST_Node* node){
    indexLambdas(node);
    Env environment(nullptr);
    processNode(node, environment);
    std::cout<<std::endl;
    return CSENode(NodeType::INTEGER);
    //return environment.RStack.top();
}

void CSE::processChildren(AST_Node* node, Env& environment){
    AST_Node* it = node->leftchild;
    while(it->next != nullptr){
        it = it->next;
    }
    while(it != nullptr){
        processNode(it, environment);
        it = it->prev;
    }
}

void CSE::processNode(AST_Node* node, Env& environment){
    switch(node->type){
        case NodeType::LAMBDA:
            processLAMBDA(node, environment);
            break;

        case NodeType::TAU:
            processChildren(node, environment);
            processTAU(node, environment);
            break;

        case NodeType::ARROW:
            processARROW(node, environment);
            break;

        case NodeType::B_OR:
        case NodeType::B_AND:
        case NodeType::B_GR:
        case NodeType::B_GE:
        case NodeType::B_LS:
        case NodeType::B_LE:
        case NodeType::B_EQ:
        case NodeType::B_NE:
        case NodeType::ADD:
        case NodeType::SUB:
        case NodeType::MULT:
        case NodeType::DIV:
        case NodeType::POW:
        case NodeType::AUG:
            processChildren(node, environment);
            processBINARY(node->type, environment);
            break;


        case NodeType::NEG:
        case NodeType::B_NOT:
            processChildren(node, environment);
            processUNARY(node->type, environment);
            break;



        case NodeType::IDENTIFIER:
            processIDENTIFIER(node->text, environment);
            break;

        case NodeType::INTEGER: {
            std::stringstream ss(node->text); int my_res; ss >> my_res;
            processINTEGER(my_res, environment);
        } break;

        case NodeType::STRING:
            processSTRING(node->text, environment);
            break;

        case NodeType::R_TRUE:
        case NodeType::R_FALSE:
        case NodeType::R_NIL:
        case NodeType::R_DUMMY:
        case NodeType::YSTAR:
            processDUMMY(node->type, environment);
            break;

        case NodeType::GAMMA:
            processChildren(node, environment);
            processGAMMA(environment);
            break;

        default:
            throw "tree not optimized??";
            break;
    }
}


void CSE::processLAMBDA(AST_Node* node, Env& environment){
    CSENode temp(NodeType::LAMBDA);
    temp.baseEnv = &environment;
    temp.node = node;
    environment.RStack.push(temp);
}

void CSE::processTAU(AST_Node* node, Env& environment){
    int count = 1;
    for(auto it = node->leftchild; it->next != nullptr; it = it->next)
        count++;
    CSENode temp(NodeType::TAU);
    for(int it = 0; it < count; it++){
        temp.tauValue.push(environment.RStack.top());
        environment.RStack.pop();
    }
    environment.RStack.push(temp);
}

void CSE::processARROW(AST_Node* node, Env& environment){
    auto E = node->leftchild;
    auto delt = E->next;
    auto delf = delt->next;

    processNode(E, environment);
    auto temp = environment.RStack.top().type;
    environment.RStack.pop();
    if(temp == NodeType::R_TRUE){
        processNode(delt, environment);
        return;
    } else if (temp == NodeType::R_FALSE){
        processNode(delf, environment);
        return;
    } else {
        throw "Arrow expression did not evaluate to boolean";
    }
}

void CSE::processUNARY(NodeType type, Env& environment){
    auto temp = environment.RStack.top();
    environment.RStack.pop();
    switch(type){
        case NodeType::NEG: {
            if(temp.type == NodeType::INTEGER){
                CSENode newNode(NodeType::INTEGER);
                newNode.intValue = (-1)*temp.intValue;
                environment.RStack.push(newNode);
            } else
                throw "Integer expected";
        } break;
        case NodeType::B_NOT: {
            if(temp.type == NodeType::R_TRUE){
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            } else if(temp.type == NodeType::R_FALSE){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                throw "Boolean value expected";
            }
        } break;
        default: break;
    }
}

void CSE::processBINARY(NodeType type, Env& environment){
    auto left = environment.RStack.top(); environment.RStack.pop();
    auto right = environment.RStack.top(); environment.RStack.pop();
    switch(type){
        case NodeType::B_OR: {
            if((left.type != NodeType::R_TRUE && left.type != NodeType::R_FALSE) || (right.type != NodeType::R_TRUE && right.type != NodeType::R_FALSE))
                throw "Bool expected";
            if(left.type == NodeType::R_TRUE || right.type == NodeType::R_TRUE){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            }
        } break;
        case NodeType::B_AND: {
            if((left.type != NodeType::R_TRUE && left.type != NodeType::R_FALSE) || (right.type != NodeType::R_TRUE && right.type != NodeType::R_FALSE))
                throw "Bool expected";
            if(left.type == NodeType::R_TRUE && right.type == NodeType::R_TRUE){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            }
        } break;
        case NodeType::B_GR: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            if(left.intValue > right.intValue){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            }
        } break;
        case NodeType::B_GE: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            if(left.intValue >= right.intValue){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            }
        } break;
        case NodeType::B_LS: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            if(left.intValue < right.intValue){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            }
        } break;
        case NodeType::B_LE: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            if(left.intValue <= right.intValue){
                CSENode newNode(NodeType::R_TRUE);
                environment.RStack.push(newNode);
            } else {
                CSENode newNode(NodeType::R_FALSE);
                environment.RStack.push(newNode);
            }
        } break;
        case NodeType::B_EQ: {
            if(left.type == NodeType::INTEGER && right.type == NodeType::INTEGER){
                if(left.intValue == right.intValue){
                    CSENode newNode(NodeType::R_TRUE);
                    environment.RStack.push(newNode);
                } else {
                    CSENode newNode(NodeType::R_FALSE);
                    environment.RStack.push(newNode);
                }
            } else if(left.type == NodeType::STRING && right.type == NodeType::STRING){
                if(left.strValue == right.strValue){
                    CSENode newNode(NodeType::R_TRUE);
                    environment.RStack.push(newNode);
                } else {
                    CSENode newNode(NodeType::R_FALSE);
                    environment.RStack.push(newNode);
                }
            } else
                throw "Int or string arguments expected";
        } break;
        case NodeType::B_NE: {
            if(left.type == NodeType::INTEGER && right.type == NodeType::INTEGER){
                if(left.intValue != right.intValue){
                    CSENode newNode(NodeType::R_TRUE);
                    environment.RStack.push(newNode);
                } else {
                    CSENode newNode(NodeType::R_FALSE);
                    environment.RStack.push(newNode);
                }
            } else if(left.type == NodeType::STRING && right.type == NodeType::STRING){
                if(left.strValue != right.strValue){
                    CSENode newNode(NodeType::R_TRUE);
                    environment.RStack.push(newNode);
                } else {
                    CSENode newNode(NodeType::R_FALSE);
                    environment.RStack.push(newNode);
                }
            } else
                throw "Int or string arguments expected";
        } break;
        case NodeType::ADD: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            CSENode newNode(NodeType::INTEGER);
            newNode.intValue = left.intValue + right.intValue;
            environment.RStack.push(newNode);
        } break;
        case NodeType::SUB: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            CSENode newNode(NodeType::INTEGER);
            newNode.intValue = left.intValue - right.intValue;
            environment.RStack.push(newNode);
        } break;
        case NodeType::MULT: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            CSENode newNode(NodeType::INTEGER);
            newNode.intValue = left.intValue * right.intValue;
            environment.RStack.push(newNode);
        } break;
        case NodeType::DIV: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            CSENode newNode(NodeType::INTEGER);
            newNode.intValue = left.intValue / right.intValue;
            environment.RStack.push(newNode);
        } break;
        case NodeType::POW: {
            if(left.type != NodeType::INTEGER || right.type != NodeType::INTEGER)
                throw "Int expected";
            CSENode newNode(NodeType::INTEGER);
            newNode.intValue = std::pow(left.intValue, right.intValue);
            environment.RStack.push(newNode);
        } break;
        case NodeType::AUG: {
            if(left.type == NodeType::TAU){
                left.tauValue.push(right);
                environment.RStack.push(left);
            } else {
                CSENode temp(NodeType::TAU);
                if(left.type != NodeType::R_NIL)
                    temp.tauValue.push(left);
                temp.tauValue.push(right);
                environment.RStack.push(temp);
            }
        } break;
        default: break;
    }
}

void CSE::processIDENTIFIER(std::string name, Env& environment){
    if(environment.isStored(name)){
        auto temp = environment.lookup(name);
        environment.RStack.push(temp);
    }
    else if(name == "Isinteger" ||
        name == "Istruthvalue" ||
        name == "Isstring" ||
        name == "Istuple" ||
        name == "Isfunction" ||
        name == "Isdummy" ||
        name == "Stem" ||
        name == "Stern" ||
        name == "Conc" ||
        name == "Order" ||
        name == "ItoS" ||
        name == "Print"){
        CSENode temp(NodeType::IDENTIFIER);
        temp.strValue = name;
        environment.RStack.push(temp);
    } else
        throw "unrecognized identifier";
}

void CSE::processINTEGER(int value, Env& environment){
    CSENode temp(NodeType::INTEGER);
    temp.intValue = value;
    environment.RStack.push(temp);
}

void CSE::processSTRING(std::string value, Env& environment){
    CSENode temp(NodeType::STRING);
    temp.strValue = std::string(value.begin() + 1, value.end() - 1);

    auto found = temp.strValue.find("\\n");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\n");

    found = temp.strValue.find("\\r");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\r");

    found = temp.strValue.find("\\t");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\t");

    found = temp.strValue.find("\\\\");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\\");

    found = temp.strValue.find("\\\'");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\'");

    found = temp.strValue.find("\\\"");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\"");

    found = temp.strValue.find("\\\?");
    if (found!=std::string::npos)
        temp.strValue.replace(found, 2, "\?");

    environment.RStack.push(temp);
}

void CSE::processDUMMY(NodeType type, Env& environment){
    CSENode temp(type);
    environment.RStack.push(temp);
}

void CSE::processGAMMA(Env& environment){
    auto operation = environment.RStack.top(); environment.RStack.pop();
    switch(operation.type){
        case NodeType::LAMBDA: {
            auto newEnv = new Env(operation.baseEnv);
            auto values = environment.RStack.top(); environment.RStack.pop();
            auto variable = operation.node->leftchild;
            if(variable->type == NodeType::IDENTIFIER){
                newEnv->store(variable->text, values);
            } else if(variable->type == NodeType::COMMA){
                if(values.type != NodeType::TAU) throw "Arguments not in a tuple";
                auto it = variable->leftchild;
                while(it->next != nullptr) it = it->next;
                while(it != nullptr){
                    if(values.tauValue.size() == 0) throw "more variables than values";
                    auto tempVal = values.tauValue.top(); values.tauValue.pop();
                    newEnv->store(it->text, tempVal);
                    it = it->prev;
                }
                if(values.tauValue.size() > 0) throw "more values than variables";
            } else if(variable->type == NodeType::BRKT){}
            else throw "variable list expected";
            processNode(variable->next, *newEnv);
            if(newEnv->RStack.size() > 0){
                CSENode temporary = newEnv->RStack.top();
                environment.RStack.push(temporary);
            }
        } break;
        case NodeType::ETA: {
            environment.RStack.push(operation);
            operation.type = NodeType::LAMBDA;
            environment.RStack.push(operation);
            processGAMMA(environment);
            processGAMMA(environment);
        } break;
        case NodeType::YSTAR: {
            if(environment.RStack.top().type == NodeType::LAMBDA){
                environment.RStack.top().type = NodeType::ETA;
            } else
                throw "LAMBDA expected";
        } break;
        case NodeType::IDENTIFIER: {
            auto argument = environment.RStack.top(); environment.RStack.pop();
            if(operation.strValue == "Isinteger") {
                if(argument.type == NodeType::INTEGER){
                    CSENode temp(NodeType::R_TRUE);
                    environment.RStack.push(temp);
                } else {
                    CSENode temp(NodeType::R_FALSE);
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Istruthvalue") {
                if(argument.type == NodeType::R_TRUE || argument.type == NodeType::R_FALSE){
                    CSENode temp(NodeType::R_TRUE);
                    environment.RStack.push(temp);
                } else {
                    CSENode temp(NodeType::R_FALSE);
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Isstring") {
                if(argument.type == NodeType::STRING){
                    CSENode temp(NodeType::R_TRUE);
                    environment.RStack.push(temp);
                } else {
                    CSENode temp(NodeType::R_FALSE);
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Istuple") {
                if(argument.type == NodeType::TAU || argument.type == NodeType::R_NIL){
                    CSENode temp(NodeType::R_TRUE);
                    environment.RStack.push(temp);
                } else {
                    CSENode temp(NodeType::R_FALSE);
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Isfunction") {
                if(argument.type == NodeType::LAMBDA || argument.type == NodeType::IDENTIFIER){
                    CSENode temp(NodeType::R_TRUE);
                    environment.RStack.push(temp);
                } else {
                    CSENode temp(NodeType::R_FALSE);
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Isdummy") {
                if(argument.type == NodeType::R_DUMMY){
                    CSENode temp(NodeType::R_TRUE);
                    environment.RStack.push(temp);
                } else {
                    CSENode temp(NodeType::R_FALSE);
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Stem") {
                if(argument.type != NodeType::STRING) throw "expected STRING";
                CSENode temp(NodeType::STRING);
                temp.strValue = std::string(argument.strValue.begin(), argument.strValue.begin()+1);
                environment.RStack.push(temp);
            } else if(operation.strValue == "Stern") {
                if(argument.type != NodeType::STRING) throw "expected STRING";
                CSENode temp(NodeType::STRING);
                temp.strValue = std::string(argument.strValue.begin()+1, argument.strValue.end());
                environment.RStack.push(temp);
            } else if(operation.strValue == "Conc") {
                if(!operation.isPrimed){
                    operation.isPrimed = true;
                    environment.RStack.push(argument);
                    environment.RStack.push(operation);
                } else {
                    auto argument2 = environment.RStack.top(); environment.RStack.pop();
                    if(argument.type != NodeType::STRING || argument2.type != NodeType::STRING) throw "expected STRING";
                    CSENode temp(NodeType::STRING);
                    temp.strValue = argument.strValue + argument2.strValue;
                    environment.RStack.push(temp);
                }
            } else if(operation.strValue == "Order") {
                CSENode temp(NodeType::INTEGER);
                if(argument.type == NodeType::TAU)
                    temp.intValue = argument.tauValue.size();
                else if (argument.type == NodeType::R_NIL)
                    temp.intValue = 0;
                else
                    throw "expected TAU or NIL";
                environment.RStack.push(temp);
            } else if(operation.strValue == "ItoS") {
                if(argument.type != NodeType::INTEGER) throw "expected INTEGER";
                CSENode temp(NodeType::STRING);
                temp.strValue = std::to_string(argument.intValue);
                environment.RStack.push(temp);
            } else if(operation.strValue == "Print") {
                printNode(argument);
                CSENode temp(NodeType::R_NIL);
                environment.RStack.push(temp);
            } else throw "unexpected identifier";
        } break;
        case NodeType::TAU: {
            auto argument = environment.RStack.top(); environment.RStack.pop();
            if(argument.type != NodeType::INTEGER) throw "expected integer";
            if(argument.intValue > (int)operation.tauValue.size()) throw "invalid index";
            int waste = operation.tauValue.size() - argument.intValue;
            for(int i = 0; i < waste; i++, operation.tauValue.pop());
            auto temporary = operation.tauValue.top();
            environment.RStack.push(temporary);
        } break;
        default:
            throw "unexpected operand for gamma";
            break;
    }
}

void CSE::printElements(std::stack<CSENode>& list){
    if(list.size() > 1){
        auto temp = list.top(); list.pop();
        printElements(list);
        std::cout<<", ";
        printNode(temp);
    } else {
        auto temp = list.top(); list.pop();
        printNode(temp);
    }
}

void CSE::printNode(CSENode& argument){
    if(argument.type == NodeType::INTEGER){
        std::cout<<argument.intValue;
    } else if(argument.type == NodeType::STRING){
        std::cout<<argument.strValue;
    } else if(argument.type == NodeType::TAU){
        std::cout<<'(';
        printElements(argument.tauValue);
        std::cout<<')';
    } else if(argument.type == NodeType::R_TRUE){
        std::cout<<"true";
    } else if(argument.type == NodeType::R_FALSE){
        std::cout<<"false";
    } else if(argument.type == NodeType::LAMBDA){
        std::cout<<"[lambda closure: ";
        if(argument.node->leftchild->type == NodeType::IDENTIFIER)
            std::cout<<argument.node->leftchild->text;
        else
            std::cout<<"list of variables <unsupported as of now>";
        std::cout<< ": " << argument.node->lIndex << "]";
    }
}

}
