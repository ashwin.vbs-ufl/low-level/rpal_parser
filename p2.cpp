#include "RPALCSE.h"
#include <iostream>

using namespace RPAL;

std::string getNodeString(NodeType type){
    switch(type){
        case NodeType::LET: return "let"; break;
        case NodeType::LAMBDA: return "lambda"; break;
        case NodeType::WHERE: return "where"; break;
        case NodeType::TAU: return "tau"; break;
        case NodeType::AUG: return "aug"; break;
        case NodeType::ARROW: return "->"; break;
        case NodeType::B_OR: return "or"; break;
        case NodeType::B_AND: return "&"; break;
        case NodeType::B_NOT: return "not"; break;
        case NodeType::B_GR: return "gr"; break;
        case NodeType::B_GE: return "ge"; break;
        case NodeType::B_LS: return "ls"; break;
        case NodeType::B_LE: return "le"; break;
        case NodeType::B_EQ: return "eq"; break;
        case NodeType::B_NE: return "ne"; break;
        case NodeType::ADD: return "+"; break;
        case NodeType::SUB: return "-"; break;
        case NodeType::NEG: return "neg"; break;
        case NodeType::MULT: return "*"; break;
        case NodeType::DIV: return "/"; break;
        case NodeType::POW: return "**"; break;
        case NodeType::AT: return "@"; break;
        case NodeType::GAMMA: return "gamma"; break;
        case NodeType::R_TRUE: return "<true>"; break;
        case NodeType::R_FALSE: return "<false>"; break;
        case NodeType::R_NIL: return "<nil>"; break;
        case NodeType::R_DUMMY: return "<dummy>"; break;
        case NodeType::D_WITHIN: return "within"; break;
        case NodeType::D_AND: return "and"; break;
        case NodeType::D_REC: return "rec"; break;
        case NodeType::D_EQ: return "="; break;
        case NodeType::D_FCN: return "function_form"; break;
        case NodeType::BRKT: return "()"; break;
        case NodeType::COMMA: return ","; break;

        case NodeType::IDENTIFIER: return "ID"; break;
        case NodeType::INTEGER: return "INT"; break;
        case NodeType::STRING: return "STR"; break;
        case NodeType::YSTAR: return "<Y*>"; break;

        case NodeType::ETA: return "<ETA>"; break;
    }
    return "invalid case";
}

//recursive function to output the AST
void printNode(AST_Node* node, int level){
    int num = level;

    //print preceeding dots
    while(num > 0){
        std::cout<<'.';
        num--;
    }

    //print the node data
    if(node->type == NodeType::IDENTIFIER || node->type == NodeType::INTEGER || node->type ==  NodeType::STRING)
        std::cout<< "<" << getNodeString(node->type) << ":" << node->text << ">" << std::endl;
    else
        std::cout<< getNodeString(node->type) << std::endl;

    //print child nodes
    if(node->leftchild != nullptr)
        printNode(node->leftchild, level + 1);

    //print sibling nodes
    if(node->next != nullptr)
        printNode(node->next, level);
}

int main(int argc, char *argv[]){
    int num = argc - 1;
    bool ast = false, noout = false, st = false, l = false;
    std::string filename = "";

    //set flags and file name from the command line arguments
    while(num > 0){
        std::string argument = std::string(argv[argc - num]);
        if(argument[0] == '-'){
            if(argument == "-ast")
                ast = true;
            else if(argument == "-noout")
                noout = true;
            else if(argument == "-st")
                st = true;
            else if(argument == "-l")
                l = true;
        }
            else filename = argument;
        num--;
    }

    // quit program if file name is not set.
    if(filename == "")
        return 0;

    Parser file(filename);

    //build and output AST
    if(l){
        std::ifstream sourcefile(filename);
        std::string line;
        while(std::getline(sourcefile, line))
            std::cout<<line<<std::endl;
        sourcefile.close();
    }

    if(ast){
        auto node = file.buildAST();
        printNode(node, 0);
    }

    if(st){
        file.buildAST();
        auto node = file.optimize();
        printNode(node, 0);
    }

    if(!noout){
        file.buildAST();
        auto node = file.optimize();
        CSE cse;
        cse.processTree(node);
    }
    return 0;
}
