project folder description:

* ./rpalParser - root directory for the parser project. run make and make cl here. p1 program is generated here.

* ./rpalParser/RPALLexer.{h, cpp} - class files that implement the lexer and associated structures.

* ./rpalParser/RPALParser.{h, cpp} - class files that implement the parser and AST structures.

* ./rpalParser/p1.cpp - main program that handles commandline input, instantiates parser and traverses generated AST to print output.

* ./rpalParser/test/* - programs used to test various stages of the program during debug. these files are not necessary for p1 program execution.


makefile notes:

* makefile has been made a bit more verbose to ensure proper build dependencies are maintianed. additional files (*.d) are generated during compilation to store these dependencies.

scope for improvement:

1. pass const references between functi1. ons instead of objects to reduce memory footprint.

1. move the AST management functions to a separate class for better encapsulation.

1. -l flag that prints out the script was optional and has not yet been implemented.

1. lexicon and grammar does not check for validity. It assumes that input is a valid RPAL script.

1. Any exceptions that are thrown in code are not handled - These exeptions are thrown purely for debug purposes.


bugs: None*

*all the scripts tested so far, pass. test coverage not known to be complete.