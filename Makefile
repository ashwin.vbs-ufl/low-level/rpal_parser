
CPPFLAGS += -Wall -Werror -std=c++11 -g3

SRCS = RPALLexer.cpp RPALParser.cpp RPALCSE.cpp
OBJS = $(SRCS:.cpp=.o)
DEPS = $(SRCS:.cpp=.d)

.PRECIOUS : %.o %.d

all: p2


-include $(DEPS) p2.d

p2: p2.o $(OBJS)
	g++ -o p2 p2.o $(OBJS)

clean:
	rm -f $(OBJS) $(DEPS) p2.o p2.d p2

cl:
	rm -f $(OBJS) $(DEPS) p2.o p2.d p2

%.o: %.cpp
	g++ -c $(CPPFLAGS) $*.cpp -o $*.o
	@g++ -MM -MG -MP -MF $*.d -MT '$*.o' $(CPPFLAGS) $*.cpp
